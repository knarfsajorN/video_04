import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import Checkbox from './checkbox';

import check from '../images/ok.png';
import cancel from '../images/error.png';
import reset from '../images/refresh.png';

const Container = styled.div`
    display:flex;
    justify-content:center;
    align-items:center;
    flex-direction:row;
   
`;

const ContainerV2 = styled.div`
    display:flex;
    flex-direction:row;
    justify-content:center;
    align-items:center;
    margin-top : 15px;
    padding-top:15px;
    padding-bottom:15px; 
`;

interface options {
    icon : string;
    onClick: () => void;
}

interface params {
    enableReset : boolean;
    enableCancel : boolean;
}

const App = ():JSX.Element => {
    // const handleReset = () => {
    //     console.log("vamos a resetear" );
    //     if(typeof onReset === 'function') onReset()
    // };

    // const handleOk = () => {
    //     console.log("vamos a Aceptar" );
    //     if(typeof onAccept === 'function') onAccept()
    // };

    // const handleCancel = () => {
    //     console.log("vamos a Cancelar" );
    //     if(typeof onCancel === 'function') onCancel()
    // };

    // let data : options[] = [
    //     {icon:cancel, onClick:handleCancel},
    //     {icon:reset, onClick: handleReset },
    //     {icon:check, onClick: handleOk}
    // ];

    return (
        <ContainerV2  style={{borderTop: '1px solid #ffffff80'}}>
            {/* {data.map((v,i) =><Checkbox image={v.icon} onClick={v.onClick}/> )} */}
        </ContainerV2>
    );
  };
  
  export default App;